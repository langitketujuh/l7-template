var plasma = getApiVersion(1)

// Center Krunner on screen
const krunner = ConfigFile('krunnerrc')
krunner.group = 'General'
krunner.writeEntry('FreeFloating', true);

// Change keyboard repeat delay from default 600ms to 250ms
const kbd = ConfigFile('kcminputrc')
kbd.group = 'Keyboard'
kbd.writeEntry('RepeatDelay', 250);

// Create Top Panel
const panel = new Panel
panel.alignment = "left"
panel.floating = false
panel.height = Math.round(gridUnit * 1.6);

panel.location = "top"

// var plasmaappmenu = panel.addWidget("org.kde.plasma.appmenu")

// Add Window Title
var windowtitle = panel.addWidget("org.kde.windowtitle")

// Add Expandable Spacer
var spacer = panel.addWidget("org.kde.plasma.panelspacer")

// Color Picker
panel.addWidget("org.kde.plasma.colorpicker")

// System Tray
panel.addWidget("org.kde.plasma.systemtray")

// Digital Clock
var digitalclock = panel.addWidget("org.kde.plasma.digitalclock")
digitalclock.currentConfigGroup = ["Appearance"]
digitalclock.writeConfig("showDate", false)
digitalclock.writeConfig("use24hFormat", 0)
digitalclock.writeConfig("showWeekNumbers", true)

// User Switcher
var switcher = panel.addWidget("org.kde.plasma.userswitcher")
switcher.currentConfigGroup = ["General"]
switcher.writeConfig("showFace", true)
switcher.writeConfig("showName", false)
switcher.writeConfig("showTechnicalInfo", true)

