var plasma = getApiVersion(1)

// Create bottom panel (Dock) //

const dock = new Panel

// Basic Dock Geometry
dock.alignment = "center"
dock.floating = true
dock.height = Math.round(gridUnit * 3.0)

// Dock hiding "dodgewindows" or "alwaysvisible"
dock.hiding = "alwaysvisible"

dock.lengthMode = "fit"
dock.location = "bottom"

// The Menu launcher
var launcher = dock.addWidget("org.kde.menu.ditto")
launcher.currentConfigGroup = ["General"]
launcher.writeConfig("customButtonImage", "distributor-logo-langitketujuh")
launcher.writeConfig("displayPosition", 2)
launcher.writeConfig("favoritesPortedToKAstats", true)
launcher.writeConfig("icon", "distributor-logo-langitketujuh")
launcher.writeConfig("numberColumns", 5)
launcher.writeConfig("numberRows", 4)
launcher.writeConfig("removeApplicationCommand", "")
launcher.writeConfig("showAtCenter", true)
launcher.writeConfig("showFavoritesFirst", true)
launcher.writeConfig("showInfoUser", false)
launcher.writeConfig("useCustomButtonImage", true)
launcher.writeConfig("favoriteApps", "org.kde.dolphin.desktop,preferred://browser,ardour6.desktop,arduino.desktop,audacity.desktop,blender.desktop,cadence.desktop,carla.desktop,com.obsproject.Studio.desktop,displaycal.desktop.fontforge.FontForge.desktop,fr.handbrake.ghb.desktop,gimp.desktop,gmic_qt.desktop,godot.desktop,goxel.desktop,hugin.desktop,io.github.OpenToonz.desktop,kde.kid3-qt.desktop,librecad.desktop,lmms.desktop,net.fasterland.converseen.desktop,openscad.desktop,org.bunkus.mkvtoolnix-gui.desktop,org.freecad.FreeCAD.desktop,org.inkscape.Inkscape.desktop,org.kde.digikam.desktop,org.kde.kdenlive.desktop,org.kde.krita.desktop,org.synfig.SynfigStudio.desktop,rawtherapee.desktop,scribus.desktop,langitketujuh.system.upgrade.desktop")

// Margin Separator
var separator = dock.addWidget("org.kde.plasma.marginsseparator")

// Icons-Only Task Manager
var tasks = dock.addWidget("org.kde.plasma.icontasks")
tasks.currentConfigGroup = ["General"]
tasks.writeConfig("fill", false)
tasks.writeConfig("iconSpacing", 0)
tasks.writeConfig("launchers", "applications:org.kde.dolphin.desktop,preferred://browser,applications:gimp.desktop,applications:org.inkscape.Inkscape.desktop,applications:libreoffice-startcenter.desktop,applications:org.telegram.desktop.desktop,applications:systemsettings.desktop,applications:langitketujuh.system.upgrade.desktop")
tasks.writeConfig("maxStripes", 1)
tasks.writeConfig("showOnlyCurrentDesktop", false)
tasks.writeConfig("showOnlyCurrentScreen", false)

// Margin Separator
var separator = dock.addWidget("org.kde.plasma.marginsseparator")

// Trash
var trash = dock.addWidget("org.kde.plasma.trash")

// End of Dock creation //
